function docker_bash -d "Open a Bash in the given Docker container (by ID)"
    docker exec -t -i $argv /bin/bash
end
