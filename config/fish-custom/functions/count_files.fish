function count_files -d "Recursively count files in the given directory"
    set -l count (find $argv -type f | wc -l)
    printf "Recursively found %d files in %s" $count $argv
end
