This local fish plugin contains my custom functions and config files to keep
them separate from other plugins installed through the Fisher plugin manager
(which installs plugins in the standard conf.d/ and functions/ directories).
