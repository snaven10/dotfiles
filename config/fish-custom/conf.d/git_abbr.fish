# Abbreviations for working with Git.
#
# Author: Benedikt Vollmerhaus
# License: MIT

if status --is-interactive
    abbr -a gcl 'git clone'
    abbr -a gf 'git fetch'
    abbr -a gm 'git merge'
    abbr -a gcp 'git cherry-pick'

    # Stage
    abbr -a ga 'git add'
    abbr -a gall 'git add -A'
    abbr -a grm 'git rm'

    # Commit
    abbr -a gc 'git commit'
    abbr -a gca 'git commit -a'
    abbr -a gcm 'git commit -m'
    abbr -a gcam 'git commit -am'
    abbr -a gcma 'git commit -am'
    abbr -a gci 'git commit -i'

    # Push/Pull
    abbr -a gp 'git push'
    abbr -a gl 'git pull'
    abbr -a gpl 'git pull'
    abbr -a gpp 'git pull && git push'

    # Checkout
    abbr -a gco 'git checkout'
    abbr -a gcob 'git checkout -b'
    abbr -a gcom 'git checkout master'

    # Tag
    abbr -a gt 'git tag'
    abbr -a gta 'git tag -a'
    abbr -a gtd 'git tag -d'
    abbr -a gtl 'git tag -l'

    # Other
    abbr -a gd 'git diff'
    abbr -a gs 'git status'
    abbr -a gss 'git status -s'

    # Submodules
    abbr -a gsm 'git submodule'
    abbr -a gsmu 'git submodule update --recursive --remote'
end
